# Подразумевалось, что вы уже прочли инструкции в example.rpy

init python:
    _granberia = Mod(
        "Этот текст всё равно никто не увидит. Можете использовать это поле как описание",
        "granberia",
        # Метку для таких модов можете не указывать
        None
    )
    # Если вы хотите сделать мод, меняющий геймплей, вам скорее всего не нужно,
    # чтобы он отображался в списке побочек. Для этого просто не делайте mods.append
    # Точно такого же эффекта можно добиться, указав в качестве стартовой метки None

    # Чтобы мы могли отключать этот мод, давайте добавим переключатель
    # Пока отключим, т.к. мод из стандартной поставки
    _granberia.enable = False

    # Давайте для примера изменим здоровье и опыт Гранберии в первой битве
    # Для начала свяжем оригинальную метку с нашей
    # И засунем это под условие, чтобы движок не мапил нашу метку, если вдруг мы отключим мод
    if _granberia.enable:
        config.label_overrides = {
            "granberia_ng_start": "_granberia_granberia_ng_start"
        }
    

if _granberia.enable:
    label _granberia_granberia_ng_start:
        python:
            world.troops[2] = Troop(world)
            world.troops[2].enemy[0] = Enemy(world)

            world.troops[2].enemy[0].name = Character("Гранберия")
            world.troops[2].enemy[0].sprite = ["granberia st41", "granberia st42", "granberia st07"]
            world.troops[2].enemy[0].position = center
            world.troops[2].enemy[0].defense = 70
            world.troops[2].enemy[0].evasion = 80
            # Увеличим в два раза её HP
            world.troops[2].enemy[0].max_life = world.troops[2].battle.difficulty(64000, 80000, 104000)
            # И особое событие при достижении того или иного здоровья
            world.troops[2].enemy[0].henkahp[0] = world.troops[2].battle.difficulty(16000, 18000, 20000)
            world.troops[2].enemy[0].power = world.troops[2].battle.difficulty(0, 1, 2)

            world.troops[2].battle.tag = "granberia_ng"
            world.troops[2].battle.name = world.troops[2].enemy[0].name
            # Давайте поменяем фон у битвы
            world.troops[2].battle.background = "bg iliasforest"
            # И музыку, раз уж сюда полезли!
            # Какой ID у того или иного трека смотрите в Battle.musicplay
            world.troops[2].battle.music = 7
            world.troops[2].battle.ng = True
            # Увеличим опыт за битву в два раза
            world.troops[2].battle.exp = 3000000
            world.troops[2].battle.exit = "lb_0008"

            world.troops[2].battle.init(0)

            world.party.player.earth_keigen = 50
            world.party.player.skill_set(2)

        # Перед началом битвы нам нужно будет сбалансить её, так что давайте дадим Луке Саламандру
        $ world.party.player.skill[4] = 3

        # Остальной код придётся продублировать, т.к он тоже находится в granberia_ng_start

        pause .5

        l "Тебе же лучше сдаться, Гранберия!{w}\nПрекрати нападать на невинных людей и захватывать города! В противном случае я буду вынужден применить силу!"
        g "Ты доволно самоуверен, не так ли?{w}\nЧто ж, посмотрим на что ты способен!"

        $ world.battle.show_skillname("Рассекающий удар")
        play sound "se/karaburi.ogg"

        "Гранберия взмахивает своим мечом, кося всё на своём пути!"

        $ world.battle.show_skillname("Шаг в сторону")
        play sound "se/miss.ogg"
        $ renpy.show(world.battle.background, at_list=[miss2])

        "Но Лука легко уклоняется от удара просто отшагнув!"

        $ world.battle.hide_skillname()
        show granberia st04

        g "Хмпф.{w}\nЭто сложнее, чем я думала..."
        l "Ты действительно надеялась, что это будет так легко?"

        show granberia st02

        g "Не зазнавайся!"

        show granberia st41

        g "Как тебе такое?!"

        $ world.battle.show_skillname("Удар Дракона-Мясника")
        play sound "se/karaburi.ogg"

        "Меч Гранберии обрушивается на Луку!"

        pause .3
        $ world.battle.show_skillname("Безмятежное движение")
        play sound "se/miss_aqua.ogg"
        $ renpy.show(world.battle.background, at_list=[miss2])

        "Но Лука уклоняется, словно поток воды, обегающий препятствие!"

        $ world.battle.hide_skillname()
        show granberia st06

        g "Что?..{w} Это движение!"

        show granberia st07

        g "Где ты ему научился?!{w}{nw}"

        show granberia st41

        g "Не важно, хватит уже этих игр."

        show granberia st04

        $ world.battle.show_skillname("Безмятежный Демонический Клинок")
        play sound "se/aqua2.ogg"

        "Гранберия вкладывает свой меч в ножны.{w}{nw}"

        show granberia st41
        $ world.party.player.skill13a()

        extend "\nИ резко вынимая, прорезает всё на своём пути!"

        $ world.battle.show_skillname("Безмятежное движение")
        play sound "se/miss_aqua.ogg"
        $ renpy.show(world.battle.background, at_list=[miss2])

        "Но Лука едва уходит от атаки!"

        $ world.battle.hide_skillname()
        show granberia st07

        g "!!!"
        l "Я не позволю тебе захватить Илиасбург!"

        $ world.battle.counter()
        $ world.battle.show_skillname("Денница")

        "Ослепительная звезда обрушивается в ад!"

        python:
            world.party.player.skill22a()
            for line in range(3):
                renpy.sound.play("se/damage2.ogg")
                world.party.player.damage = rand().randint(3200,3600)
                if not line:
                    _window_show()
                    renpy.show_screen("hp")
                    _window_auto = True

                world.battle.enemy[0].raw_life_calc(line+1)

            world.battle.hide_skillname()

        show granberia st06

        g "!!!{w}\nЭта сила!.."
        "На мгновение Гранберия застыла в неподвижной позе, шокированная моей атакой."

        show granberia st04
        stop music fadeout 1.0

        g "Хорошо.{w}\nЕсли ты так силён..."

        show granberia st41

        if persistent.music:
            # Здесь тоже поменяем
            $ world.battle.music = 32

        $ world.battle.musicplay(world.battle.music)

        g "Тогда время игр закончено, пацан!"

        $ world.battle.show_skillname("Ярость")
        play sound "se/fire4.ogg"
        show fire
        show granberia st42
        show effect flame
        $ world.battle.enemy[0].sprite[0] = "granberia st42"
        pause .5
        hide fire
        hide effect flame

        "Языки пламени пробегают по мечу Гранберии, воспламеняя её дух!"

        $ world.battle.enemy[0].life_regen(world.battle.enemy[0].max_life - world.battle.enemy[0].life)
        $ world.battle.hide_skillname()

        g "Покажи свои лучшие приёмы, малыш!{w}\nТебе они понадобятся!"
        l "С удовольствием..."
        "Я до сих пор не совсем понимаю, что тут творится...{w}\nНо если мне снова придётся победить Гранберию, то так тому и быть!"

        if persistent.difficulty == 3:
            $ world.battle.nostar = 1
            if _in_replay:
                $ world.party.player.skill[0] = 26

        $ world.battle.main()
