Моды устанавливаются в виде папок в директорию `game/mods`.

Список модов:

* [example](https://gitlab.com/Darkness9724/mgq-ng-ru-mods/-/archive/master/mgq-ng-ru-mods-master.zip?path=example) — содержит базовые примеры для мода. Также предоставляет наиболее оптимальную структуру.
* [granberia](https://gitlab.com/Darkness9724/mgq-ng-ru-mods/-/archive/master/mgq-ng-ru-mods-master.zip?path=granberia) — пример мода, который меняет непосредственно геймплей NG+. Конкретно этот битву с Гранберией. Можно развить вплоть до изменения всех аспектов игры.
